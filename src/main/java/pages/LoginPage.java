package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;
import utility.GetPropertyfile;

public class LoginPage {

	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public LoginPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	@FindBy (xpath="//*[@id=\"serviceNumber\"]")
	WebElement emailid;
	
	@FindBy (xpath="//*[@id=\"serviceNumber\"]")
	WebElement Nextbtnemailid;
	
	@FindBy (xpath="//*[@id=\"serviceNumber\"]")
	WebElement Password;
	
	@FindBy (xpath="//h2[@class='mainHeading']")
	WebElement DashboardHeader;
	
	@FindBy (xpath="//input[@type='email']")
	WebElement EnterEmailId;
	
	@FindBy (xpath="//input[@class='btn btn-block btn-primary']")
	WebElement NextEmail;
	
	@FindBy (xpath="//input[@name='passwd']")
	WebElement EnterPassword;
	
	@FindBy (xpath="//input[@type='submit']")
	WebElement SignIn;
	
	@FindBy (xpath="//input[@type='submit']")
	WebElement Yes;
	
	@FindBy (xpath="//div[@id='passwordError']")
	WebElement PasswordError;
	
	

	
public void LoginDetails() throws AWTException,FilloException, InterruptedException, IOException {
	
	
	Thread.sleep(5000);
	String title = driver.getTitle();
	System.out.println("title----------"+title);
	if(title.equals("Sign in to your account")) {
	
	EnterEmailId.click();
	String email_id = rs.getField("UserName");
	EnterEmailId.sendKeys(email_id);
	logger.log(LogStatus.PASS, "Entered User Email id "+email_id);  
	NextEmail.click();
	Thread.sleep(2000);
	EnterPassword.click();
	//String password = rs.getField("Password");
	GetPropertyfile gp = new GetPropertyfile();
	String password = gp.getProperty("url_password");
	EnterPassword.sendKeys(password);
	logger.log(LogStatus.PASS, "Entered Password"); 
	SignIn.click();
	Thread.sleep(3000);
	//Yes.click();
	Thread.sleep(15000);
	
	}else {
		
		driver.navigate().refresh();
		Thread.sleep(3000);
	}
		
	}
	

	public void Validate_SuccessfulLogin() throws AWTException, InterruptedException {
		
		Thread.sleep(5000);
		DashboardHeader.isDisplayed();
		logger.log(LogStatus.PASS, "Login Successful"); 
		Thread.sleep(2500);
		
		
	}
	
	public void Validate_InvalidLogin() throws AWTException, InterruptedException {
		
		Thread.sleep(10000);
		PasswordError.isDisplayed();
		String error = PasswordError.getText();
		drawBorder(driver,PasswordError);
		logger.log(LogStatus.PASS, "Application access is restricted with Invalid Login with error message.."+ error); 
		Thread.sleep(2500);
		
		
	}

	private void drawBorder(WebDriver driver2, WebElement passwordError2) {
		// TODO Auto-generated method stub
		
	}
}
