package pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;

public class ContactPage {

	
	
	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public ContactPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	
	@FindBy (xpath="(//mat-card-title[@class='mat-card-title'])[1]")
	WebElement custCount;
	
	@FindBy (xpath="(//mat-card-title[@class='mat-card-title'])[2]")
	WebElement pnrCount;
	
	
	public void fn_SelectDepartureCityStart() throws InterruptedException, FilloException, AWTException {
		
		
		String cust_count = custCount.getText();
		String pnr_count = pnrCount.getText();
			
			Thread.sleep(2000);

	
}
	
	
	
	
}
