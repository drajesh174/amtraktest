package pages;

import java.awt.AWTException;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;

public class PreImpactSummaryPage {

	

	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public PreImpactSummaryPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	
	
	@FindBy (xpath="//span[contains(text(),'SUBMIT')]")
	WebElement submitbtn;
	
	@FindBy (xpath="//button[@class='close_icon']")
	WebElement closepopup;
	
	

	@FindBy (xpath="//button[@class='toolkitButton cursorPointer']")
	WebElement toolkit;
	
	@FindBy (xpath="//span[contains(text(),'Assign/Reassign')]")
	WebElement assign;
	
	@FindBy (xpath="//input[@id='mat-input-9']")
	WebElement assigneeName;
	
	@FindBy (xpath="//span[contains(text(),'ASSIGN')]")
	WebElement assignconf;
	
	@FindBy (xpath="//span[contains(text(),'OK')]")
	WebElement okbtn;
	
	@FindBy (xpath="//span[contains(text(),'Save My Work')]")
	WebElement savemywork;
	
	@FindBy (xpath="//span[contains(text(),'Save and Exit')]")
	WebElement saveandexit;
	
	
	
	public void fn_clickSubmit() throws InterruptedException {
	
		logstatus.logScreen(driver, logger, "PreSubmit Page. ");
		submitbtn.click();
		Thread.sleep(4000);
}
	
	public void fn_closePopp() throws InterruptedException {
		
		logstatus.logScreen(driver, logger, "Cancellation Submitted/Fail. ");
		closepopup.click();
		Thread.sleep(4000);
		
		logstatus.logScreen(driver, logger, "Dashboard Page ");
		
		Thread.sleep(4000);
		
		driver.navigate().refresh();
    }
	
	public void fn_Assign_Reassign() throws InterruptedException, FilloException {
		
		toolkit.click();
		Thread.sleep(2000);
		assign.click();
		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Clicked on Assign/Reassign "); 
		String assignName = rs.getField("Assign_Reassign");
		assigneeName.sendKeys(assignName);
		driver.findElement(By.xpath("//b[contains(text(),'"+assignName+"')]")).click();
		logstatus.logScreen(driver, logger, "Assign/Reassign ");
		assignconf.click();
		Thread.sleep(5000);
		okbtn.click();
		Thread.sleep(2000);
		
    }
		
	public void fn_saveMyWork() throws InterruptedException {
		
		toolkit.click();
		Thread.sleep(2000);
		savemywork.click();
		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Clicked on Saved my Work "); 
		
		logstatus.logScreen(driver, logger, "Save My work Functionality ");
		closepopup.click();
		Thread.sleep(2000);
		
		
    }
	
	
	public void fn_saveAndExit() throws InterruptedException {
		
		toolkit.click();
		Thread.sleep(2000);
		saveandexit.click();
		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Clicked Save and Exit "); 
		
		logstatus.logScreen(driver, logger, "Save and Exit functionality. ");
		okbtn.click();
		Thread.sleep(2000);
		
		
    }
	
}
