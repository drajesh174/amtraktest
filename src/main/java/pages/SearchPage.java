package pages;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;

public class SearchPage {

	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public SearchPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	@FindBy(xpath="//button[@class='mat-button' and @value='Export to XLS']")
	WebElement exporttoxls;
	
	@FindBy(xpath="//button[@class='toolkitButton cursorPointer']")
	WebElement toolkitbutton;
	
	
	@FindBy (xpath="//button[@class='search_text cursorPointer ng-star-inserted']")
	WebElement searchbtn;
	
	@FindBy (xpath="//mat-select[@id='mat-select-0']//div[@class='mat-select-arrow']")
	WebElement serviceDetails;
	
	
	@FindBy (xpath="//span[contains(text(),'SCR ID')]")
	WebElement scrId;
	
	@FindBy(xpath= "//span[contains(text(),'Service Name')]")
	WebElement serviceName;
	
	@FindBy(xpath = "//span[contains(text(),'Service Number')]")
	WebElement serviceNumber;
	
	@FindBy(xpath="//input[@id='serviceName']")
	WebElement serviceNameText;
	
	@FindBy (xpath="//input[@id='scrId']")
	WebElement scrIdtext;
	
	@FindBy (xpath="//span[@class='mat-button-wrapper']")
	WebElement search;

	@FindBy (xpath="//div[@class='textALignRight rightArrow']")
	WebElement openSCR;
	
	@FindBy (xpath="//*[@class='textWhite link' and contains(text(),'Home')]")
	WebElement clickHome;
	
	@FindBy(xpath = "//*[@id='mat-select-5']/div/div[2]/div")
	WebElement scrstatusdrop;
	
	@FindBy (xpath ="//span[@class='mat-option-text' and contains(text(),'Notify')]")
	WebElement scrstatusnotify;
	
	
	@FindBy(xpath = "//span[@class='mat-option-text' and contains(text(),'In Progress')]")
	WebElement scastatusinprogress;
	
	@FindBy(xpath = "//span[@class='mat-option-text' and contains(text(),'Processing')]")
	WebElement scastatusprocessing;
	
	@FindBy(xpath= "//span[@class='mat-option-text' and contains(text(),'Completed')]")
	WebElement scastatuscompleted;
	
	
	@FindBy(xpath= "//span[@class='mat-option-text' and contains(text(),'Abandoned')]")
	WebElement scastatusabandon;
	
	@FindBy(xpath= "//span[@class='mat-option-text' and contains(text(),'Failed')]")
	WebElement scastatusfailed;
	
	@FindBy(xpath = "//span[@class='mat-option-text' and contains(text(),'None')]")
	WebElement scastatusnone;
	
	
	@FindBy(xpath="//mat-radio-button[@value='assignee']")
	WebElement assigneeradio;
	
	@FindBy(xpath="//input[@id='assignee']")
	WebElement assigneetext;
	
	@FindBy(xpath="//div[@class='col-md-4 textALignRight']/span[2]")
	WebElement statuschk;
	
	
	public void fn_searchSCRId() throws Exception {

		clickHome.click();
		Thread.sleep(7000);
		searchbtn.click();
		Thread.sleep(1000);
		serviceDetails.click();
		Thread.sleep(1000);
		scrId.click();
		Thread.sleep(1000);
		String sca_id = rs.getField("SCA_id");
		Thread.sleep(500);
		scrIdtext.sendKeys(sca_id);
		Thread.sleep(1000);
		search.click();
		Thread.sleep(7000);
		openSCR.click();
		Thread.sleep(7000);
		logstatus.logScreen(driver, logger, "Search by SCR_ID is successfully");
		logger.log(LogStatus.PASS, "Search by SCR_ID is successfully          ");  
		System.out.println("**********Search by SCR_ID is successfully*********");
}
	
	
	public void fn_searchServiceName() throws Exception{
		clickHome.click();
		Thread.sleep(7000);
		searchbtn.click();
		Thread.sleep(1000);
		serviceDetails.click();
		Thread.sleep(1000);
		serviceName.click();
		Thread.sleep(1000);
		String sca_id = rs.getField("SCA_id");
		Thread.sleep(500);
		serviceNameText.sendKeys(sca_id);
		Thread.sleep(1000);
		search.click();
		Thread.sleep(7000);
		openSCR.click();
		Thread.sleep(7000);
		logstatus.logScreen(driver, logger, "Search by Service Name is successfully");
		logger.log(LogStatus.PASS, "Search by Service Name is successfully          ");  
		System.out.println("**********Search by Service Name is successfully*********");
}
	
	public void fn_searchSCRStatus() throws Exception{
		
		String search_By = rs.getField("SCA_id");
		Thread.sleep(5000);
		clickHome.click();
		Thread.sleep(5000);
		searchbtn.click();
		Thread.sleep(5000);
		scrstatusdrop.click();
		Thread.sleep(2000);
		if(search_By.equalsIgnoreCase("Notify")){
			scrstatusnotify.click();
		}
		else if (search_By.equalsIgnoreCase("In Progress")){
			scastatusinprogress.click();
		}
		else if (search_By.equalsIgnoreCase("Processing")){
			scastatusprocessing.click();
		}
		
		else if (search_By.equalsIgnoreCase("Completed")){
			scastatuscompleted.click();
		}
		else if (search_By.equalsIgnoreCase("Abandoned")) {
			scastatusabandon.click();	
		}
		else if(search_By.equalsIgnoreCase("Failed")){
			scastatusfailed.click();
		}
		else{
			scastatusnone.click();
		}
		
		Thread.sleep(500);
		search.click();
		logstatus.logScreen(driver, logger, "Search by SCR  is successfully");
		logger.log(LogStatus.PASS, "Search by SCR  is successfully          ");  
		System.out.println("**********Search by SCR  is successfully*********");
	}
	
	
	public void fn_searchAssignee() throws Exception{
		
		String search_By = rs.getField("SCA_id");
		clickHome.click();
		Thread.sleep(5000);
		searchbtn.click();
		Thread.sleep(5000);
		assigneeradio.click();
		Thread.sleep(500);
		assigneetext.sendKeys(search_By);
		search.click();
		Thread.sleep(4000);
		logstatus.logScreen(driver, logger, "Search by Assignee  is successfully");
		logger.log(LogStatus.PASS, "Search by Assignee  is successfully          ");  
		System.out.println("**********Search by Assignee  is successfully*********");
		
	}
	
	
	
	public void fn_generateReport() throws Exception{
		
		Thread.sleep(2000);
		toolkitbutton.click();
		Thread.sleep(200);
		exporttoxls.click();
		Thread.sleep(10000);
		
		String downloadPath = "C:\\Users\\813077\\Downloads";
		File getLatestFile = getLatestFilefromDir(downloadPath);
	    String fileName = getLatestFile.getName();
	    System.out.println(fileName);
		
	}
	
	private File getLatestFilefromDir(String dirPath){
	    File dir = new File(dirPath);
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) {
	        return null;
	    }
	
	    File lastModifiedFile = files[0];
	    for (int i = 1; i < files.length; i++) {
	       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	           lastModifiedFile = files[i];
	       }
	    }
	    return lastModifiedFile;
	}
	
	
	
	public boolean fn_searchSCRIdNotify(String srcid) throws Exception {

		boolean statuscode = false;
		clickHome.click();
		Thread.sleep(7000);
		searchbtn.click();
		Thread.sleep(1000);
		serviceDetails.click();
		Thread.sleep(1000);
		scrId.click();
		Thread.sleep(1000);
		scrIdtext.sendKeys(srcid);
		Thread.sleep(1000);
		search.click();
		Thread.sleep(7000);
		logstatus.logScreen(driver, logger, "Search by SCR_ID is successfully");
		logger.log(LogStatus.PASS, "Search by SCR_ID is successfully          ");  
		System.out.println("**********Search by SCR_ID is successfully*********");
		String status = statuschk.getText();
		System.out.println(status);
		if(status.equals("Notify")) {
			statuscode=true;
			openSCR.click();
			Thread.sleep(7000);
		}
		return statuscode;
		
}
	
	
	
	
	}
	
	
	
	
	
	
	
