package pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;

public class CreateRequestPage {

	
	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public CreateRequestPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	@FindBy (xpath="//span[contains(text(),'Create')]")
	WebElement createLink;
	
	//span[contains(text(),'Create')] 
	
	@FindBy (xpath="//*[@id=\"serviceNumber\"]")
	WebElement serviveNo;
	
	@FindBy (xpath="//*[@id=\"serviceForm\"]/button")
	WebElement nextButton;
	
	@FindBy (xpath="//*[@id=\"serviceForm\"]/div/span")
	WebElement errorMsg;
	
	
	public void fn_EnterServiceNO() throws InterruptedException, FilloException, AWTException {
		
		createLink.click();
		String servicr_No = rs.getField("ServiceNumber");
		System.out.println(servicr_No);
		serviveNo.sendKeys(servicr_No);
		Thread.sleep(4000);
		logstatus.logScreen(driver, logger, "Create Request. ");
		nextButton.click();
		Thread.sleep(2000);
		
	
}
	
	public void fn_validateServiceNo() throws InterruptedException {
		
		System.out.println("fn_validateServiceNo....");
		createLink.click();
		Thread.sleep(2000);
		serviveNo.sendKeys("0");
		Thread.sleep(1000);
		String error = errorMsg.getText();
		drawBorder(driver,errorMsg);
		logger.log(LogStatus.PASS, "Service number cannot start with 0");  
		logger.log(LogStatus.PASS, "Displayed Error message --->  "+error);  
		logstatus.logScreen(driver, logger, "Error message. ");
		serviveNo.clear();
		
		Thread.sleep(2000);
		serviveNo.sendKeys("23a4");
		Thread.sleep(1000);
		String error1 = errorMsg.getText();
		drawBorder(driver,errorMsg);
		logger.log(LogStatus.PASS, "Service number cannot contain letters");  
		logger.log(LogStatus.PASS, "Displayed Error message --->  "+error1);  
		logstatus.logScreen(driver, logger, "Error message. ");
		serviveNo.clear();
		
		Thread.sleep(2000);
		serviveNo.sendKeys("23 4");
		Thread.sleep(1000);
		String error2 = errorMsg.getText();
		drawBorder(driver,errorMsg);
		logger.log(LogStatus.PASS, " Service number cannot contain spaces");  
		logger.log(LogStatus.PASS, "Displayed Error message --->  "+error2);  
		logstatus.logScreen(driver, logger, "Error message. ");
		serviveNo.clear();
		
		Thread.sleep(2000);
		serviveNo.sendKeys("12345");
		Thread.sleep(1000);
		String error3 = errorMsg.getText();
		drawBorder(driver,errorMsg);
		logger.log(LogStatus.PASS, " Service number cannot be longer than four characters");  
		logger.log(LogStatus.PASS, "Displayed Error message --->  "+error3);  
		logstatus.logScreen(driver, logger, "Error message. ");
		//serviveNo.clear();
		 
	}
	
	
	public static void drawBorder(WebDriver driver, WebElement element_node){
       // WebElement element_node = driver.findElement(By.xpath(xpath));
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].style.border='3px solid red'", element_node);
    }
	
	
	
	
}
