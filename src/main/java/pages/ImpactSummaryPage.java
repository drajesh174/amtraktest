package pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;
import utility.DatabaseValidation;
import utility.SDHApiValidation;
import utility.UpdateExcel;
import utility.WARReportValidation;

public class ImpactSummaryPage {

	
	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public ImpactSummaryPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	SDHApiValidation sdhval = new SDHApiValidation();
	
	WARReportValidation warval = new WARReportValidation();
	

	
	@FindBy (xpath="//span[contains(text(),'CONTINUE')]")
	WebElement continuebtn;
	
	@FindBy (xpath="//mat-card-header[1]//mat-card-title")
	WebElement customercnt;
	
	@FindBy (xpath="//mat-card-header[2]//mat-card-title")
	WebElement pnrcnt;
	
	@FindBy (xpath="//div[@class='mat-slide-toggle-thumb']")
	WebElement reaccomodationtoggle;
	
	@FindBy (xpath="//input[@id='serviceNumber']")
	WebElement reaccomodateServiceNumber;
	
	@FindBy (xpath="//span[contains(text(),'Lookup')]")
	WebElement lookupbtn;
	
	@FindBy (xpath="//span[contains(text(),'OK')]")
	WebElement okbtn;
	
	@FindBy (xpath="//span[@class='close_icon']")
	WebElement closeIcon;
	
	@FindBy (xpath="//p[@class='statusText']")
	WebElement scaid;
	

	@FindBy (xpath="//button[@class='toolkitButton cursorPointer']")
	WebElement toolkit;
	
	@FindBy (xpath="//span[contains(text(),'ABANDON')]")
	WebElement abandonbtn;
	
	@FindBy (xpath="(//span[contains(text(),'ABANDON')])[2]")
	WebElement abandonbtnConf;
	
	@FindBy (xpath="//div[contains(text(),'Completed')]")
	WebElement completeTab;
	
	@FindBy (xpath="//div[@class='cardLayoutContainer']//div[1]//div[1]//div[2]//div[3]")
	WebElement abandonscr;
	
	
	
	public static String cust = "12345";
	public static String pnr = "12345";
	public static String scr_id ="12214";
	
	
	public void fn_getScaid() throws InterruptedException, FilloException, AWTException {
		
		Thread.sleep(3000);
		scr_id = scaid.getText();
		logger.log(LogStatus.INFO, "SCA id is --> "+scr_id); 
		String[] parts = scr_id.split("#");
		//String part0 = parts[0];
		String part1 = parts[1];
		scr_id = part1;
			
		}
	
	public String PassSCRID(){	
		
		return scr_id;				
	}	
	
	
public void fn_CustPNRCnt() throws InterruptedException, FilloException, AWTException {
		
	Thread.sleep(3000);

	cust = customercnt.getText();
	pnr = pnrcnt.getText();
	logger.log(LogStatus.PASS, "Customer Count in Impactsummary Page is --> "+cust); 
	logger.log(LogStatus.PASS, "PNR count in Impactsummary Page is --> "+pnr); 
	
		
	}
	
public String PassCust(){	
return cust;	
}	
	
public String PassPNR(){	
return pnr;	
}	

	
	
public void fn_Reaccomodation() throws InterruptedException, FilloException, AWTException {
		
	String reaccomodate = rs.getField("Reaccomodate");
	
	if(reaccomodate.equals("Yes")) 
	{
	
	
		reaccomodationtoggle.click();
		Thread.sleep(3000);	
		String reaccServNo = rs.getField("ReaccomodateServiceNumber");
		//reaccomodateServiceNumber.click();
		//Thread.sleep(1000);
		reaccomodateServiceNumber.sendKeys(reaccServNo);
		lookupbtn.click();
		Thread.sleep(15000);
		
	
	}
	}


public void fn_clickContinueImp() throws InterruptedException, FilloException, AWTException {
	
	logstatus.logScreen(driver, logger, "Impact Summary. ");
	
	try {
	continuebtn.click();
	Thread.sleep(4000);	
	}catch(Exception ex) {
		
		System.out.println("Rejected By arrow");
		closeIcon.click();
		Thread.sleep(2000);
		continuebtn.click();
		Thread.sleep(4000);	
	}
		
	
}

public void fn_getScaIdAndDBValidation() throws InterruptedException, FilloException, AWTException {
	
	
	System.out.println("sar id is -->" + scr_id);
	
	DatabaseValidation dv = new DatabaseValidation();
	String status = dv.validateStatus(scr_id);	
	
}

public void fn_validateEmail() throws Exception {
	
	
	System.out.println("sar id is -->" + scr_id);
	
	DatabaseValidation dv = new DatabaseValidation();
	dv.EmailNotification(scr_id);	
	
}



public void fn_ValidateSDHApi() throws Exception {
	
	 String snumber  = rs.getField("ServiceNumber");
	 String dateCancellation = rs.getField("StartDate");
	System.out.println(cust);
	System.out.println(snumber);
	//sdhval.sdhDataValidate(cust,snumber,dateCancellation);	
	sdhval.SDHValidation(cust,pnr,snumber,dateCancellation);	
	
	
}


public void fn_ValidateTicketApi() throws Exception {
	
	 String snumber  = rs.getField("ServiceNumber");
	 String dateCancellation = rs.getField("StartDate");
	System.out.println(cust);
	System.out.println(snumber);
	//DatabaseValidation dv = new DatabaseValidation();
	//String pnr1 = dv.validatePostImpact(scr_id);  // no need.........................
	
	SearchPage spage = new SearchPage(driver, logger, rs);
	boolean statuscode = spage.fn_searchSCRIdNotify(scr_id);
	if(statuscode) {
	String pnr1 = pnrcnt.getText();
	warval.WARReportTest(snumber,dateCancellation,pnr1);	
	}
	
}



	public void fn_updateExcel() throws Exception {	
	
	
		System.out.println("sar id is -->" + scr_id);	
		System.out.println("UI Customer count is -->" +cust);	
		System.out.println("UI PNR count is -->" +pnr);	
	
		UpdateExcel uex = new UpdateExcel();	
		uex.excelUpdate_scaid(scr_id);		
		uex.excelupdate_sdhcust(cust);	
		uex.executeupdate_sdhpnr(pnr);	
	
		logger.log(LogStatus.INFO, "Values updated in Testdata Excel sheet"); 	
	
	}


public void fn_validateAbandon() throws InterruptedException, FilloException, AWTException {
	
	abandonbtn.click();
	logger.log(LogStatus.PASS, "Abandon button clicked"); 
	Thread.sleep(2000);
	logstatus.logScreen(driver, logger, "Abandon functionality Validation. ");
	abandonbtnConf.click();
	Thread.sleep(3000);
	completeTab.click();
	Thread.sleep(2000);
	abandonscr.click();
	Thread.sleep(5000);
	logstatus.logScreen(driver, logger, "Abandon SCR. ");
	
	
}


public static void drawBorder(WebDriver driver, String xpath){
    WebElement element_node = driver.findElement(By.xpath(xpath));
    JavascriptExecutor jse = (JavascriptExecutor) driver;
    jse.executeScript("arguments[0].style.border='3px solid red'", element_node);
}


	
}
