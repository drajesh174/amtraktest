package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;


public class CancellationDetailsPage {

	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	public CancellationDetailsPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	
	

	
	@FindBy (xpath="(//input[contains(@id,'mat-input-1')])")
	WebElement fromDate;
	//input[contains(@id,'mat-input')])[2]
	
	@FindBy (xpath="(//input[contains(@id,'mat-input-2')])")
	WebElement toDate;
	
	@FindBy (xpath="(//div[@class='mat-button-ripple mat-ripple mat-button-ripple-round'])[2]")
	WebElement nextMonthbtn;
	
	@FindBy (xpath="//div[@class='cdk-live-announcer-element cdk-visually-hidden']")
	WebElement cmonth;
	
	
	
	@FindBy (xpath="//*[@id=\"mat-checkbox-2\"]/label/div")
	WebElement cancelAllCheckBox;
	
	@FindBy (xpath="//div[contains(text(),'Everyday')] ")
	WebElement everydaybtn;
	 
	@FindBy (xpath="//div[contains(text(),'Custom')] ")
	WebElement custombtn;
	 
	
	@FindBy (xpath="//div[contains(text(),'Partial')]")
	WebElement partialCancellationBtn;
	
	
	@FindBy (xpath="(//mat-select[contains(@id,'mat-select-')]//div[@class='mat-select-arrow'])[1]")
	WebElement DepartureCityDropDown;
	 
	
	@FindBy (xpath="(//mat-select[contains(@id,'mat-select-')]//div[@class='mat-select-arrow'])[2]")
	WebElement ArrivalCityDropdown;
	
	@FindBy (xpath="//div[@class='mat-select-arrow']")
	WebElement reasonTypeFull;
	
	@FindBy (xpath="(//div[@class='mat-select-arrow'])[3]")
	WebElement reasonTypePar;
	
	//input[@id='mat-input-9']
	
	@FindBy (xpath="//textarea[@id='commentArea']")
	WebElement addComment;
	
	@FindBy (xpath="//span[contains(text(),'CONTINUE')]")
	WebElement continuebtn;
	
	@FindBy (xpath="//tbody[@class='mat-calendar-body']/tr[1]/td[1]")
	WebElement monthcal;
	
	

	@FindBy (xpath="//button[@class='toolkitButton cursorPointer']")
	WebElement toolkit;
	
	@FindBy (xpath="//span[contains(text(),'Clear All')]")
	WebElement clearAll;
	
	@FindBy (xpath="//span[contains(text(),'Yes')]")
	WebElement clearAllConf;
	
	
	@FindBy (xpath="//span[contains(text(),'DISCARD')]")
	WebElement discardbtn;
	 
	
	
	//--- Select single date-----------
	public void fn_SelectStartDate() throws InterruptedException, FilloException, AWTException, ParseException {
		
		fromDate.click();
		Thread.sleep(2000);
		String sdate = rs.getField("StartDate");
		System.out.println(sdate);
		logger.log(LogStatus.PASS, "Start date is "+sdate); 
		String[] parts = sdate.split("/");
		String part0 = parts[0];
		String part1 = parts[1];
		String part2 = parts[2];
		
		String year = "20"+part2;
	
		
		for(int k=1;k<=10;k++) {
			
			String currentMonth = cmonth.getText();
			System.out.println("Current month ------------------------->"+currentMonth);
			String[] months_part = currentMonth.split(" ");
			String months_part0 = months_part[0];
			String months_part1 = months_part[1];
			
			String month_no = findMonth(months_part0);
			
			if(months_part1.equals(year) && month_no.equals(part0) ) {
				
				System.out.println("You are in Correct Month......");
				break;
				
			}else {
				
				nextMonthbtn.click();
				Thread.sleep(3000);
				System.out.println("Clicked on next month");
			}
		}
		
		System.out.println(part0);
		System.out.println(part1);
		
		//Boolean isPresent = driver.findElements(By.yourLocator).size() > 0
			
		Thread.sleep(1000);
		String c_category = rs.getField("Category");
		if(c_category.equalsIgnoreCase("Unplanned")) {
			
			driver.findElement(By.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today'][text() ='"+part1+"']")).click();
		}
		else {
		driver.findElement(By.xpath("//div[@class='mat-calendar-body-cell-content'][text() ='"+part1+"']")).click();
		}
				//div[@class='mat-calendar-body-cell-content'][text() ='19']
/*
	outerloop:
	for(int i=1;i<=6;i++) {
		
		for(int j=1;j<=7;j++)
		{
			//try:
			
			WebElement	day = driver.findElement(By.xpath("//tr["+i+"]//td["+j+"]/div"));
			
			String day1 = day.getText();
			System.out.println(day1);
			try {
			if(day1.equals(part1)) {
				day.click();
				break outerloop;
			}
			}catch(org.openqa.selenium.StaleElementReferenceException ex) {
				day.click();
				break outerloop;
			}
			
			
			//except IndexError:
				
			
		}
		
	}
	
		*/

		//String edate = rs.getField("EndDate");
       // toDate.click();
        
		logger.log(LogStatus.PASS, "End date is "+sdate); 
		System.out.println("Date selected");
		Thread.sleep(2000);
}
	
	//--- Select Multiple date-----------
	public void fn_SelectEndDate() throws InterruptedException, FilloException, AWTException {
		
		toDate.click();
		Thread.sleep(2000);
		String edate = rs.getField("EndDate");
		System.out.println(edate);
		logger.log(LogStatus.PASS, "Start date is "+edate); 
		String[] parts = edate.split("/");
		String part0 = parts[0];
		String part1 = parts[1];
		String part2 = parts[2];
	

	outerloop:
	for(int i=2;i<=6;i++) {
		
		for(int j=1;j<=7;j++)
		{
			
			
			WebElement	day = driver.findElement(By.xpath("//tr["+i+"]//td["+j+"]/div"));
			
			String day1 = day.getText();
			System.out.println(day1);
			try {
			if(day1.equals(part1)) {
				day.click();
				break outerloop;
			}
			}catch(org.openqa.selenium.StaleElementReferenceException ex) {
				day.click();
				break outerloop;
			}
		}
		
	}
		
     
        
        
		logger.log(LogStatus.PASS, "End date is "+edate); 
		System.out.println("Date selected");
		Thread.sleep(2000);
	
}


	
	
	//------ Custom Selection------------
public void fn_SelectCustom() throws InterruptedException, FilloException, AWTException, ParseException {
		
	custombtn.click();
	Thread.sleep(2000);
	String sdate = rs.getField("StartDate");
	String edate = rs.getField("EndDate");
	
	
		
	String day1 = findDays(sdate);	  
	driver.findElement(By.xpath("//span[./text()='"+day1+"']")).click();
	
	Thread.sleep(1000);
	System.out.println("Selected day1 ---"+day1);
	
	
	for(int i=1;i<=7;i++) {
		
		
			String[] parts = sdate.split("/");
			String part0 = parts[0];
			String part1 = parts[1];
			String part2 = parts[2];
			int result = Integer.parseInt(part1);
			result=result+1;
			String fresult = Integer.toString(result);
			String ndate = part0+"/"+fresult+"/"+part2;
			System.out.println("nday---"+ndate);
			String dayn=findDays(ndate);
			driver.findElement(By.xpath("//span[./text()='"+dayn+"']")).click();
			//driver.findElement(By.xpath("//span[contains(text(), '"+dayn+"')]")).click();
			System.out.println("Selected dayn ---"+dayn);
			sdate=ndate;
			
			if(sdate.equals(edate)) {
				System.out.println("start date and end date are same");
				break;
			}
				
		
		Thread.sleep(1000);
	}
	
	Thread.sleep(1000);
	
}




	
public void fn_ClickCancellationTypeAndCity() throws InterruptedException, FilloException, AWTException {
		
	String ctype = rs.getField("CancellationType");
	if(ctype.equals("Partial")) {
	    //partialCancellationBtn.click();
		partialCancellationBtn.click();
		Thread.sleep(2000);
		fn_SelectDepartureCityStart();
		fn_SelectArrivalCity();
		
		logger.log(LogStatus.PASS, "Cancellation Type is -- Partial "); 
	}
	
	else {
		
		logger.log(LogStatus.PASS, "Cancellation Type is -- Full "); 
		Thread.sleep(1000);
	}
	
}
	
	
	public void fn_SelectDepartureCityStart() throws InterruptedException, FilloException, AWTException {
		
		
		DepartureCityDropDown.click();
		Thread.sleep(1000);
		String dstation = rs.getField("DepurtureStation");
		WebElement drstation = driver.findElement(By.xpath("//span[@class='mat-option-text'][contains(text(), '"+dstation+"')]"));
		drstation.click();
			
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "Depurture Station is -- "+dstation); 	
	
}
	
	

	public void fn_SelectArrivalCity() throws InterruptedException, FilloException, AWTException {
		
		
		ArrivalCityDropdown.click();
		
		String astation = rs.getField("ArrivalStation");
		Thread.sleep(1000);
		WebElement arrstation = driver.findElement(By.xpath("//span[@class='mat-option-text'][contains(text(), '"+astation+"')]"));
		
		arrstation.click();
		System.out.println("arrival station "+astation);

		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Arrival Station is -- "+astation); 	
}	
	
	public void fn_SelectReasonType() throws InterruptedException, FilloException, AWTException {
		
		String ctype = rs.getField("CancellationType");
		if(ctype.equals("Partial")) {
			
			reasonTypePar.click();
		}
		else {
			reasonTypeFull.click();
		}
		
		
		Thread.sleep(1000);
		String reason = rs.getField("ReasonType");
		WebElement reasontyp = driver.findElement(By.xpath("//span[contains(text(),'"+reason+"')]"));
		reasontyp.click();
		
		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Reason Type is -- "+reason); 	

}
	
	public void fn_addComment() throws InterruptedException, AWTException, FilloException  {
		
		/*
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		String text1 = rs.getField("Comments");
		//String text1 = "QA";
		StringSelection stringSelection1 = new StringSelection(text1);
		Clipboard clipboard1 = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard1.setContents(stringSelection1, stringSelection1);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(1000);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
	*/
		String comment = rs.getField("Comments");
		addComment.sendKeys(comment);
		Thread.sleep(2000);
		
		logger.log(LogStatus.PASS, "Comment Type is -- "+comment); 	

}
	
	public void fn_clickContinue() throws InterruptedException  {
		
		logstatus.logScreen(driver, logger, "Cancelation Details. ");
		continuebtn.click();
		Thread.sleep(10000);
	
		

}

	
	public void fn_validateDiscard() throws InterruptedException  {
		
			
		logstatus.logScreen(driver, logger, "Discard Button. ");
		drawBorder(driver,discardbtn);
		discardbtn.click();
		logger.log(LogStatus.PASS, "Click on Discurd Button"); 
		Thread.sleep(3000);
		logstatus.logScreen(driver, logger, "Discard button Validation. ");
		

}
	
	public void fn_validateClearAll() throws InterruptedException  {
		
		
		toolkit.click();
		Thread.sleep(1000);
		clearAll.click();
		logger.log(LogStatus.PASS, "Click on Clear All"); 
		Thread.sleep(1000);
		logstatus.logScreen(driver, logger, "Clear button Validation. ");
		clearAllConf.click();
		Thread.sleep(2000);
		logstatus.logScreen(driver, logger, "Clear All Validation. ");

}
	
	
	
	public String findDays(String sdate) throws ParseException  {
		
		 String days = null;
		String input_date= sdate;
		System.out.println(input_date);
		  SimpleDateFormat format1=new SimpleDateFormat("MM/dd/yy");
		  Date dt1=format1.parse(input_date);
		  DateFormat format2=new SimpleDateFormat("EEEE"); 
		  String finalDay=format2.format(dt1);
		  System.out.println(finalDay);
		  
		 
		 
		  if(finalDay.equals("Sunday")) {
			  days="Su";
		  }
		  
		  if(finalDay.equals("Monday")) {
			  days="M";
		  }
		  
		  if(finalDay.equals("Tuesday")) {
			  days="T";
		  }
		  
		  if(finalDay.equals("Wednesday")) {
			  days="W";
		  }
		  
		  if(finalDay.equals("Thursday")) {
			  days="Th";
		  }
		  
		  if(finalDay.equals("Friday")) {
			  days="F";
		  }
		  
		  if(finalDay.equals("Saturday")) {
			  days="S";
		  }
		  
		  return days;


	}
	
	public String findMonth(String month) throws ParseException  {
		
	
		String monthNo = null;
		
		 
		 
		  if(month.equals("JAN")) {
			  monthNo="1";
		  }
		  
		  if(month.equals("FEB")) {
			  monthNo="2";
		  }
		  
		  if(month.equals("MAR")) {
			  monthNo="3";
		  }
		  
		  if(month.equals("APR")) {
			  monthNo="4";
		  }
		  
		  if(month.equals("MAY")) {
			  monthNo="5";
		  }
		  
		  if(month.equals("JUN")) {
			  monthNo="6";
		  }
		  
		  if(month.equals("JUL")) {
			  monthNo="7";
		  }
		  
		  if(month.equals("AUG")) {
			  monthNo="8";
		  }
		  if(month.equals("SEP")) {
			  monthNo="9";
		  }
		  if(month.equals("OCT")) {
			  monthNo="10";
		  }
		  if(month.equals("NOV")) {
			  monthNo="11";
		  }
		  if(month.equals("DEC")) {
			  monthNo="12";
		  }
		  
		  return monthNo;


	}
	
	
	public static void drawBorder(WebDriver driver, WebElement element_node){
	       // WebElement element_node = driver.findElement(By.xpath(xpath));
	        JavascriptExecutor jse = (JavascriptExecutor) driver;
	        jse.executeScript("arguments[0].style.border='3px solid red'", element_node);
	    }
	
}
