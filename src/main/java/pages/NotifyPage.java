package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.CaptureScreen;

public class NotifyPage {

	WebDriver driver;
	ExtentReports extent;
	ExtentTest logger;
	Recordset rs;
	
	
public NotifyPage(WebDriver driver,ExtentTest logger,Recordset rs) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
		PageFactory.initElements(driver, this);
	}
	

	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);


	@FindBy (xpath ="//*[@class='commentsButtonWrapper']/button/span[1]")
	WebElement viewaddcomment;
	
	@FindBy(xpath ="/html/body/sca-app-root/sca-app-dashboard/mat-sidenav-container/mat-sidenav-content/sca-dashboard/div/div/sca-notify/div/div/div[2]/div[2]/p[3]/span[2]")
	WebElement notifystatus;
	
	
	@FindBy(xpath="/html/body/sca-app-root/sca-app-dashboard/mat-sidenav-container/mat-sidenav-content/sca-dashboard/div/div/sca-final-cancellation-summary/div/div/div[2]/div[2]/p[2]/span[2]")
	WebElement completestatus;
	
	
	@FindBy(xpath ="//input[@formcontrolname='actionInput']")
	WebElement actionsnotify;
	
	@FindBy(xpath="//*[@formcontrolname='commentInput']")
	WebElement commentsnotify;
	
	@FindBy(xpath ="//*[@class='mat-button-wrapper' and contains(text(),'Add')]")
	WebElement addnotify;
	
	@FindBy(xpath ="//*[@id='mat-dialog-0']/sca-alert-popup/div/button/span/span")
	WebElement commentClose;
	
	@FindBy(xpath ="//div[@class='ng-star-inserted']")
	WebElement textNoComments;
	
	@FindBy(xpath ="//*[@id='mat-checkbox-1']/label/div")
	WebElement clickallcustomernotified;
	
	@FindBy(xpath ="/html/body/sca-app-root/sca-app-dashboard/mat-sidenav-container/mat-sidenav-content/sca-dashboard/div/div/sca-notify/div/div/div[4]/div/div/sca-comments/div[3]/button/span")
	WebElement Submitclick;
	
	@FindBy(xpath= "//*[@class='ng-star-inserted' and contains(text(),' Cancellation Closed Sucessfully ')]")
	WebElement canellationsuccessmessage;
	
	@FindBy(xpath ="//*[@class='close_icon' and contains(text(),'close')]")
	WebElement closepopup;
	
	public void fn_ChangeToComplete() throws Exception{
		
		viewaddcomment.click();
		Thread.sleep(7000);
		String SCRStatus= "Notify";
		String value = notifystatus.getText();
		if(value.equalsIgnoreCase(SCRStatus)){
			
			logstatus.logScreen(driver, logger, "Selected SCR is in Notify state  ");
			actionsnotify.sendKeys("AUTOMATION TESTING");
			commentsnotify.sendKeys("QA TEST OFFSHORE");
			addnotify.click();
			Thread.sleep(7000);
			commentClose.click();
			Thread.sleep(7000);
			clickallcustomernotified.click();
			logstatus.logScreen(driver, logger, "Submit button is enable and all customers are notified  ");
			Thread.sleep(5000);
			Submitclick.click();
			Thread.sleep(5000);
			closepopup.click();
			Thread.sleep(5000);
			logger.log(LogStatus.PASS, "Submit button clicked, SCR status moved to Complete status  "); 
			}
		}
		
	
	
	
}
