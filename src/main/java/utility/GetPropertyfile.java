package utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class GetPropertyfile {

	
	public String getProperty(String key) throws IOException {
		
		
		
		// Load the properties File		
	    Properties prop = new Properties();					
	    FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\data.properties");									
	    prop.load(objfile);					
	// ------------------------------	
	  
	    String key_val = prop.getProperty(key);
		
		return key_val;
		
	
	}
	
	
}
