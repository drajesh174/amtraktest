package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import testCase.BaseClass;

public class DatabaseValidation extends BaseClass{
	
	

	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	
	static String URL = "jdbc:postgresql://scatest.cjdc6xiddbff.us-east-1.rds.amazonaws.com/scatest";
	static String user = "sca_test";
	static String password = "Scat3$t#123";
	
	//---------------------------------
	
	public  String validatePostImpact(String scaid) throws Exception{
		
		String unproctedCount = null;
		String pnrIndicator;

		String query ="SELECT * FROM sca.impact_summary where sca_id ='"+scaid+"'";
		String pnr_cnt = null;
				
		try {
			
			Connection con = DriverManager.getConnection(URL, user, password);
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs1 =ps.executeQuery();
		
		    while ( rs1.next() ) {
	           
		    	unproctedCount = rs1.getString("unprtc_cnt");
	             pnrIndicator = rs1.getString("pnr_ind");
	             System.out.println(unproctedCount);
	             System.out.println(pnrIndicator);
	            
	            if(pnrIndicator.equals("Y")) {
	            	
	            	System.out.println( "PNR = " + unproctedCount );
	            	
	            	logger.log(LogStatus.INFO, "PNR in SCA_DB is --> "+unproctedCount); 
	            	pnr_cnt = unproctedCount;
	            	
	            }else {
	            	
	            	System.out.println( "Customer = " + unproctedCount );
	            	logger.log(LogStatus.INFO, "Customer in SCA_DB is --> "+unproctedCount); 
	            }       
	            
	         }
			
		     rs1.close();
	         ps.close();
	         con.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			 System.out.println("Exception occurred: " + e);
		}
		
		
		return pnr_cnt;
	}
	
	
	//----------------------------------------------
	
	
	public  String validateStatus(String scaid) {
		
		
	//	select sts_cd from sca.schedule_change  where sca_id = '12106'
				
				String query ="select sts_cd from sca.schedule_change  where sca_id ='"+scaid+"'";
				String status = null;
		try {
			
			Connection con = DriverManager.getConnection(URL, user, password);
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs2 =ps.executeQuery();
			
			
		    while ( rs2.next() ) {
	           
	            
	             status = rs2.getString("sts_cd");
	            
	            System.out.println("Status  ----------------- > "+status);
	            if(status.equals("Notify")) {
	            	logger.log(LogStatus.PASS, "The SCR is in -- > "+ status +" state."); 
	            	//validatePostImpact(scaid);
	            }else {
	            	
	            	System.out.println( "Status is Fail/InProgress/Processing" );
	            	logger.log(LogStatus.FAIL, "The SCR is in -- > "+ status +" state."); 
	            }       
	            
	         }
			
		     rs2.close();
	         ps.close();
	         con.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			 System.out.println("Exception occurred: " + e);
		}
		return status;
		
	}
	

	
	
	
	public void EmailNotification(String scaid) throws Exception{

        String URL = "jdbc:postgresql://scatest.cjdc6xiddbff.us-east-1.rds.amazonaws.com/scatest";
        String user = "sca_test";
        String password = "Scat3$t#123"; 
        String query ="SELECT * from sca.email_notify where sca_id ='"+scaid+"'";
        try {
     
     Connection con = DriverManager.getConnection(URL, user, password);
     PreparedStatement ps = con.prepareStatement(query);
     ResultSet rs =ps.executeQuery();

     if (!rs.next() ) {
    	    System.out.println("no data");
    	    logger.log(LogStatus.FAIL, "Service cancellation email did not trigger !!");
    	    rs.beforeFirst();
    	} else { 		
    	
     do {     
             String emailSubject_database = rs.getString("email_subj");
             String Servicecanceled = "Schedule Change Request Submitted for #"+ scaid;
             String CustomerNotified = "Customers Notified for schedule change request #"+ scaid;
            if(emailSubject_database.equalsIgnoreCase(Servicecanceled)) {
             System.out.println("Service cancellation email triggered");
              logger.log(LogStatus.PASS, "Service cancellation email triggered");
    }
              else if(emailSubject_database.equalsIgnoreCase(CustomerNotified)){
          System.out.println("Customer Notified email triggered");
             logger.log(LogStatus.PASS, "Customer Notified email triggered");
      }
  
    	}while ( rs.next() );
    	}

      rs.close();
     ps.close();
       con.close();         
   } catch (Exception e) {    
     System.out.println("Exception occurred: " + e);
  }
 }



	
	
}
