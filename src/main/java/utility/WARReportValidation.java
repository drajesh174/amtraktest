package utility;


import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import testCase.BaseClass;

public class WARReportValidation extends BaseClass {

	ReuseFuns reuse = new ReuseFuns();
	UpdateExcel ux = new UpdateExcel();
	
public  void WARReportTest(String service_no, String sdate ,String pnrUI) throws Exception{
		
		SoftAssert softassertion = new SoftAssert ();
		String testName = this.getClass().getSimpleName();
		File Resultfile =reuse.createNewFile(testName);
		String fileName = Resultfile.getName();
		
		
		
		String[] parts = sdate.split("/");
		String part0 = parts[0];
		String part1 = parts[1];
		String part2 = parts[2];
		
		String monthtxt = findMonth(part0);
		
		String begindate_val = part1+monthtxt;
		
		System.out.println(begindate_val);
		
		RestAssured.baseURI = "http://arrwtpfnative.corp.nrpc:8090/rest-bin/scheduleChange/sD";
		Response response = RestAssured.given().queryParam("carrierCode", "am").queryParam("servicenumber", service_no).queryParam("begindate", begindate_val).header("X-Authorization", "846c6ea4")
				.relaxedHTTPSValidation().when().get();
		reuse.writeFile(Resultfile,response.body().asString());
		
		String filepath = System.getProperty("user.dir") + "\\" + "TestResults";
	    File file = new File(filepath + "//" + fileName);
		String str = FileUtils.readFileToString(file, "UTF-8");
		int count = StringUtils.countMatches(str, "PNRLocator");
		String pnrApi = Integer.toString(count);
		System.out.println(pnrApi);
		
		logger.log(LogStatus.INFO, "The PNR value from WAR report API --> "+pnrApi);
		softassertion.assertEquals(pnrApi, pnrUI, "The pnr value matched");
		
		if(pnrApi.equals(pnrUI))
		{
		
		logger.log(LogStatus.PASS, "The PNR values matched with WAR report");
		ux.excelUpdate_postImpact("The PNR values matched with WAR report");
		ux.excelupdate_PostImpPNRcount(pnrUI);
		ux.excelupdate_WarApiPNRcount(pnrApi);
		}
		else {
			
			logger.log(LogStatus.FAIL, "The PNR values does not matched with WAR report");
			ux.excelUpdate_postImpact("The PNR values does not matched with WAR report");
			ux.excelupdate_PostImpPNRcount(pnrUI);
			ux.excelupdate_WarApiPNRcount(pnrApi);
		}

}


public String findMonth(String smonth) throws ParseException  {
	
	 String month_val = null;

	
	 
	  if(smonth.equals("1")) {
		  month_val="jan";
	  }
	  
	  if(smonth.equals("2")) {
		  month_val="feb";
	  }
	  if(smonth.equals("3")) {
		  month_val="mar";
	  }
	  if(smonth.equals("4")) {
		  month_val="apr";
	  }
	  if(smonth.equals("5")) {
		  month_val="may";
	  }
	  if(smonth.equals("6")) {
		  month_val="jun";
	  }
	  if(smonth.equals("7")) {
		  month_val="jul";
	  }
	  if(smonth.equals("8")) {
		  month_val="aug";
	  }
	  if(smonth.equals("9")) {
		  month_val="sep";
	  }
	  if(smonth.equals("10")) {
		  month_val="oct";
	  }
	  if(smonth.equals("11")) {
		  month_val="nov";
	  }
	  if(smonth.equals("12")) {
		  month_val="dec";
	  }
	  
	  return month_val;


}
	
	
}
