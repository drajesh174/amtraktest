package utility;

import com.codoid.products.exception.FilloException;

import testCase.BaseClass;

public class GetRunStatus extends BaseClass{

	
	public boolean getrun(String tname) throws FilloException {
		
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		boolean run_st = false;
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals(tname) && runStatus.equals("Yes")) {
				
				run_st=true;
	}
	
     }
		return run_st;
	}
}
