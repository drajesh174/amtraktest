package utility;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import testCase.BaseClass;


public class SDHApiValidation extends BaseClass  {
	
	//ReuseFuns reuse = new ReuseFuns();
	
	CaptureScreen logstatus = new CaptureScreen(driver, logger, rs);
	
	UpdateExcel ux = new UpdateExcel();
	
	public void sdhDataValidate(String cust, String serviceNumber, String dateCancellation) throws Exception{
		
		// Load the properties File		
	    Properties prop = new Properties();					
	    FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\data.properties");									
	    prop.load(objfile);					
	// ------------------------------	
	  
	    String baseEndpoint = prop.getProperty("SDH_Endpoint");
	   
	    String[] parts = dateCancellation.split("/");
		String part0 = parts[0];
		String part1 = parts[1];
		String part2 = parts[2];
	    
	    String df = "20"+part2+"-"+part0+"-"+part1;
	    System.out.println(df);
	    String base_uri = baseEndpoint + "/" + serviceNumber + "/" + df;
	    RestAssured.baseURI = base_uri ;
		Response response = RestAssured.given().relaxedHTTPSValidation().when().get();
		String value = response.jsonPath().getString("totalResults");
		System.out.println(value);
		logger.log(LogStatus.INFO, "SDH Api Customer count is --> "+value); 
		
		if(value.equals(cust)) {
			
			System.out.println("SDH Api Customer count and the UI Customer count are same");
			logger.log(LogStatus.PASS, "SDH Api Customer count and the UI Customer count are same "); 
			ux.excelUpdate_imapctSummary("SDH Api Customer count and the UI Customer count are same");
		
		}else {
			
			System.out.println("SDH Api Customer count and the UI Customer count are not same");
			
			logger.log(LogStatus.FAIL, "SDH Api Customer count and the UI Customer count are not same "); 
			
			ux.excelUpdate_imapctSummary("SDH Api Customer count and the UI Customer count are not same");
		}
		
	}
	
	
	
	
	public void SDHValidation(String cust,String pnr, String serviceNumber, String dateCancellation) throws Exception{
		
		  	Properties prop = new Properties();					
		    FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\data.properties");									
		    prop.load(objfile);	
		    String baseEndpointSDH = prop.getProperty("MULESDH_Endpoint");
		    Date date = new Date(dateCancellation);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String format = formatter.format(date);
			//System.out.println(format);
	        String URI = baseEndpointSDH +serviceNumber;
	        RestAssured.baseURI = URI;
	        Response response = RestAssured.given().queryParam("startDate", format).queryParam("endDate", format).header("client_id", "c79c512e71e8434b89c73e4957e77dc9")
	                     .header("client_secret", "3d04F0716Bd74FB2B8A9D2C83d011bc5")
	                     .relaxedHTTPSValidation().when().get();
	       
	        HashMap<Object, Object> values = response.getBody().path("travelServiceCancellationImpact");
	        HashMap<Object, Object> valueOne = (HashMap<Object, Object>) values.get("travelService");
	        HashMap<Object, Object> valueTwo = (HashMap<Object, Object>) valueOne.get("impactSummary");
	        
	        Integer impactedpassengers = (Integer) valueTwo.get("impactedPassengers");
	        Integer impactedPNRs= (Integer) valueTwo.get("impactedPNRs");
	        
	        String customer = Integer.toString(impactedpassengers);
	        String pnrs = Integer.toString(impactedPNRs);
	        
	        if(customer.equals(cust) && (pnr.equals(pnrs))) {
				
				System.out.println("SDH Api Customer and PNRs count matches with the UI Customer and PNRs count");
				logger.log(LogStatus.PASS, "SDH Api Customer and PNRs count matches with the UI Customer and PNRs count"); 
				ux.excelUpdate_imapctSummary("SDH Api Customer and PNRs count matches with the UI Customer and PNRs count");
				ux.excelupdate_SDHAPIcustomercount(customer);
				ux.excelupdate_SDHAPIPNRcount(pnrs);
				
				
			
			}else {
				
				System.out.println("SDH Api Customer and PNRs count does not match with the UI Customer and PNRs count");
				
				logger.log(LogStatus.FAIL, "SDH Api Customer and PNRs count does not match with the UI Customer and PNRs count"); 
				
				ux.excelUpdate_imapctSummary("SDH Api Customer and PNRs count does not match with the UI Customer and PNRs count");
				ux.excelupdate_SDHAPIcustomercount(customer);
				ux.excelupdate_SDHAPIPNRcount(pnrs);
			}
	        
		
	}
	
	
}
