package utility;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CaptureScreen {

	WebDriver driver;
	ExtentTest logger;
	Recordset rs;
	
	public CaptureScreen (WebDriver driver, ExtentTest logger,Recordset rs ) {
		
		this.driver = driver;
		this.logger = logger;
		this.rs=rs;
	}
	
	public static String capture(WebDriver driver, String screenshotName) {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		String desc = ts.getScreenshotAs(OutputType.BASE64);
		return "data:image/jpg;base64, " +desc;
	}
	
	public void logScreen(WebDriver driver,ExtentTest logger, String objnameaction) {
		
		String path = CaptureScreen.capture(driver, objnameaction);
		String imgpath = logger.addScreenCapture(path);
		logger.log(LogStatus.PASS, objnameaction + "Screenshot for references"+ imgpath);
	}
	
	public void logstatus(WebDriver driver,ExtentTest logger,WebElement element, String objnameaction)
	{
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
			boolean status = element.isDisplayed();
			
			if(status) {
				logger.log(LogStatus.PASS, objnameaction);
			}
			
		}catch(Exception e) {
			
			String path = capture(driver, objnameaction);
			String imgpath = logger.addScreenCapture(path);
			logger.log(LogStatus.WARNING, objnameaction + "failed to identify the web page "+ imgpath);
		}
		
	}
}
