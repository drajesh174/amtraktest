package utility;

import com.codoid.products.exception.FilloException;

import testCase.BaseClass;

public class UpdateExcel extends BaseClass{

	
	public void excelUpdate_scaid(String sca_id) throws FilloException {
		
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set SCA_id='"+sca_id+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
		
		
	}
	
	public void executeupdate_sdhpnr(String pnr) throws Exception{
		
	String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set PreimpactPNRcount='"+pnr+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
	}
	
	public void excelupdate_sdhcust(String cust) throws Exception{
		
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set PreimpactCustomercount='"+cust+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);	
	}
	
	
	
	
	//Update in SDHApiValidation
	public void excelUpdate_imapctSummary(String msg) throws FilloException {
		
	
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set ImpactSummary='"+msg+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
			
	}
	
	public void excelupdate_SDHAPIcustomercount(String customer) throws Exception{
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set SDHAPIcustomercount='"+customer+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
	}
	
	public void excelupdate_SDHAPIPNRcount(String pnrs) throws Exception{
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set SDHAPIPNRcount='"+pnrs+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
	}
	
	
	
	public void excelUpdate_postImpact(String msg) throws FilloException {
		
	
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set Post_impactSumary='"+msg+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
		
		
	}
	
	
	public void excelupdate_PostImpPNRcount(String pnrs) throws Exception{
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set Post_impactPnrCnt='"+pnrs+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
	}
	
	public void excelupdate_WarApiPNRcount(String pnrs) throws Exception{
		String testname = rs.getField("TestCase_Name");
		
		String strQuery="Update Sheet1 Set WAR_API_PNR_Count='"+pnrs+"' where TestCase_Name='"+testname+"'";
		 
		connection.executeUpdate(strQuery);
	}
	
	
}
