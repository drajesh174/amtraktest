package utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class CrossBrowser {
	
	
	public static WebDriver driver;
	public static Recordset rs;
	public static com.codoid.products.fillo.Connection connection;
	
	public static void IdentifyBrowser() throws Exception  {
		
		String filepath = System.getProperty("user.dir") + "\\" + "data.properties";
		File file = new File(filepath);
		FileInputStream fileinput = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileinput);
		Fillo fillo = new Fillo();
		connection = fillo.getConnection(".\\TestData\\testdata.xlsx");
		String strQuery = "Select * from Sheet1 where Run = 'Yes'";
		rs=connection.executeQuery(strQuery);
		rs.next();
		String browser = rs.getField("Browser");
		
		
		if(browser.equalsIgnoreCase("Chrome")){
			
			System.setProperty("webdriver.chrome.driver", prop.getProperty("Chrome"));
			ChromeOptions cOption = new ChromeOptions();
			cOption.addArguments("disable-infobars");
			driver = new ChromeDriver(cOption);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.get(prop.getProperty("URL"));
		}
		
		else if (browser.equalsIgnoreCase("IE")){
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver", prop.getProperty("Internet_Exployer"));
			 WebDriver driver=new InternetExplorerDriver(capabilities);  
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.get(prop.getProperty("URL"));
		}
		
		else if (browser.equalsIgnoreCase("Firefox")){
			System.setProperty("webdriver.gecko.driver", prop.getProperty("Firefox"));
			WebDriver driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.get(prop.getProperty("URL"));
		}
		
}
	
	@Test
	public void KeywardToTestCase() throws Exception{
		Fillo fillo = new Fillo();
		connection = fillo.getConnection(".\\TestData\\testdata.xlsx");
		String strQuery = "Select * from Sheet1 where Run = 'Yes'";
		rs=connection.executeQuery(strQuery);
		rs.next();
		String keyword = rs.getField("Keyword");
		
		if(keyword.equalsIgnoreCase("Case1")){
			
			//SampleClass class = new SampleClass();
		}
			
			
		
		
		
		
	}
	
}
