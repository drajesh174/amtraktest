package utility;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import testCase.BaseClass;



public class ReuseFuns extends BaseClass {
	
	
	public static ReuseFuns reuse = new ReuseFuns();
	
	
	public  String returnTestData(String column) throws Exception{
		
		String testdata = rs.getField(column);
		return testdata;
		
	}
	
	public File createNewFile(String apiname) throws Exception {
		
		Date date = new Date();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
	    String tdate = dateFormat.format(date);
	   // String response_type = reuse.returnTestData("Response_Type");
		String filepath = System.getProperty("user.dir") + "\\" + "TestResults";
		File file = new File (filepath + "//" + apiname + "_" + tdate + ".txt");
		file.createNewFile();
		return file;
	}

	public void writeFile(File file, String value) throws Exception{
		
		FileWriter filewriter = new FileWriter(file);
		PrintWriter printwriter = new PrintWriter(filewriter);
		printwriter.print(value);
		printwriter.close();
		
		
	}
	
	/*
	
	public String databaseValidation(String query, int colIndex) throws Exception{
		String value ="";
		
		
		try {
            Class.forName("org.postgresql.Driver");
        }
        catch (java.lang.ClassNotFoundException e) {
           e.printStackTrace();
        }
        // replace below details
		
        String url = prop.getProperty("Database_scatest");
        
       // scatest.cjdc6xiddbff.us-east-1.rds.amazonaws.com
        String username = prop.getProperty("Scatest_username");
        String password = prop.getProperty("Scatest_password");

        try {
            Connection db = DriverManager.getConnection(url, username, password);
            // create object for the Statement class
            Statement st = db.createStatement();
            // execute the query on database
            ResultSet rs = st.executeQuery(query);
            System.out.println("Data retrieved from the PostgreSQL database ");
            while (rs.next()) {
            	 value = rs.getString(colIndex);
            }
            rs.close();
            st.close();
            db.close();
            }
        catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
        }
		return value;	
	}
	
	
	*/

}
