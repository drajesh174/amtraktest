package runner;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import testCase.TC01_SingleDateFull_WF1_Planned;
import testCase.TC02_SingledatePartial_WF1_Planned;
import testCase.TC03_SingleDateFull_WF1_Planned_WithReAccom;
import testCase.TC04_SingledatePartial_WF1_Planned_WithoutReAccom;
import testCase.TC05_MultiDateFull_WF1_Planned;
import testCase.TC06_MultidatePartial_WF1_Planned;
import testCase.TC07_MultiDateFull_WF1_Planned_WithReAccm;
import testCase.TC08_MultidatePartial_WF1_Planned_WithoutReAccom;
import testCase.TC09_SingleDateFull_WF2_Planned;
import testCase.TC10_SingledatePartial_WF2_Planned;
import testCase.TC11_MultiDateFull_WF2_Planned;
import testCase.TC12_MultidatePartial_WF2_Planned;
import testCase.TC13_SingleDateFull_WF3_Planned;
import testCase.TC14_SingledatePartial_WF3_Planned;
import testCase.TC15_MultiDateFull_WF3_Planned;
import testCase.TC24_ContactCentreAgent_Addcomment_NotifyCustomer;
import testCase.TC25_Toolkit_ClearAll;
import testCase.TC26_Toolkit_Assign_Reassign;
import testCase.TC27_Toolkit_SaveMyWork;
import testCase.TC28_Toolkit_SaveAndExit;
import testCase.TC29_Search_SCRId;
import testCase.TC30_Search_SCRName;

public class DynamicTestNG {
	
	public static Recordset rs;
	public static  com.codoid.products.fillo.Connection connection;

	public static void main(String[] args) throws Exception {
		
		List<XmlClass> list = new ArrayList<XmlClass>();
		XmlSuite xmlsuite = new XmlSuite();
		xmlsuite.setName("Suite");
		XmlTest xmltest = new XmlTest(xmlsuite);
		xmltest.setName("Test");
		//xmltest.setThreadCount(5);

		List arrayValue = new ArrayList();
        Fillo fillo = new Fillo();
  		connection = fillo.getConnection(".\\TestData\\testdata.xlsx");
  		String strQuery = "Select * from Sheet1 where Run = 'Yes'";
  		rs =connection.executeQuery(strQuery);
  		
  		while(rs.next()){
  			arrayValue.add(rs.getField("TestCase_Name"));
  		}
	
  		System.out.println(arrayValue);
  		
  		for (int i=0; i<arrayValue.size();i++){
  			if(arrayValue.get(i).equals("TC01_SingleDateFull_WF1_Planned")){
  				XmlClass tc01 = new XmlClass(TC01_SingleDateFull_WF1_Planned.class);
  				list.add(tc01);
  			}
  			
  			
  			if(arrayValue.get(i).equals("TC02_SingledatePartial_WF1_Planned")){
  				XmlClass tc02 = new XmlClass(TC02_SingledatePartial_WF1_Planned.class);
  				list.add(tc02);
  			}
  			
  			
  			if(arrayValue.get(i).equals("TC03_SingleDateFull_WF1_Planned_WithReAccom")){
  				XmlClass tc03 = new XmlClass(TC03_SingleDateFull_WF1_Planned_WithReAccom.class);
				list.add(tc03);
  			}
  			if(arrayValue.get(i).equals("TC04_SingledatePartial_WF1_Planned_WithoutReAccom")){
  				XmlClass tc04 = new XmlClass(TC04_SingledatePartial_WF1_Planned_WithoutReAccom.class);
				list.add(tc04);
  			}
  			if(arrayValue.get(i).equals("TC05_MultiDateFull_WF1_Planned")){
  				XmlClass tc05 = new XmlClass(TC05_MultiDateFull_WF1_Planned.class);
				list.add(tc05);
  			}
  			if (arrayValue.get(i).equals("TC06_MultidatePartial_WF1_Planned")) {
  				XmlClass tc06 = new XmlClass(TC06_MultidatePartial_WF1_Planned.class);
				list.add(tc06);
			}
  			if (arrayValue.get(i).equals("TC07_MultiDateFull_WF1_Planned_WithReAccm")) {		
  				XmlClass tc07 = new XmlClass(TC07_MultiDateFull_WF1_Planned_WithReAccm.class);
				list.add(tc07);
			}
  			
  			if (arrayValue.get(i).equals("TC08_MultidatePartial_WF1_Planned_WithoutReAccom")) {				
  				XmlClass tc08 = new XmlClass(TC08_MultidatePartial_WF1_Planned_WithoutReAccom.class);
				list.add(tc08);
			}
  			
  			if (arrayValue.get(i).equals("TC09_SingleDateFull_WF2_Planned")) {
  				XmlClass tc09 = new XmlClass(TC09_SingleDateFull_WF2_Planned.class);
				list.add(tc09);	
			}
  			if (arrayValue.get(i).equals("TC10_SingledatePartial_WF2_Planned")) { 				
  				XmlClass tc10 = new XmlClass(TC10_SingledatePartial_WF2_Planned.class);
				list.add(tc10);	
			}
  			if (arrayValue.get(i).equals("TC11_MultiDateFull_WF2_Planned")) {
  				XmlClass tc11 = new XmlClass(TC11_MultiDateFull_WF2_Planned.class);
				list.add(tc11);			
			}
  			if (arrayValue.get(i).equals("TC12_MultidatePartial_WF2_Planned")) {
  				XmlClass tc12 = new XmlClass(TC12_MultidatePartial_WF2_Planned.class);
				list.add(tc12);		
			}
  			if (arrayValue.get(i).equals("TC13_SingleDateFull_WF3_Planned")) {
  				XmlClass tc13 = new XmlClass(TC13_SingleDateFull_WF3_Planned.class);
				list.add(tc13);	
			}
  			if (arrayValue.get(i).equals("TC14_SingledatePartial_WF3_Planned")) {
  				XmlClass tc14 = new XmlClass(TC14_SingledatePartial_WF3_Planned.class);
				list.add(tc14);	
			}
  			if (arrayValue.get(i).equals("TC15_MultiDateFull_WF3_Planned")) {
  				XmlClass tc15 = new XmlClass(TC15_MultiDateFull_WF3_Planned.class);
				list.add(tc15);			
			}
  			
  			
  			/* Few class need to be added */
  			
  			
  			
  			if (arrayValue.get(i).equals("TC24_ContactCentreAgent_Addcomment_NotifyCustomer")) {
  				XmlClass tc24 = new XmlClass(TC24_ContactCentreAgent_Addcomment_NotifyCustomer.class);
				list.add(tc24);	
			}
  			if (arrayValue.get(i).equals("TC25_Toolkit_ClearAll")) {
  				XmlClass tc25 = new XmlClass(TC25_Toolkit_ClearAll.class);
				list.add(tc25);
			}
  			if (arrayValue.get(i).equals("TC26_Toolkit_Assign_Reassign")) {
  				XmlClass tc26 = new XmlClass(TC26_Toolkit_Assign_Reassign.class);
				list.add(tc26);		
			}
  			if (arrayValue.get(i).equals("TC27_Toolkit_SaveMyWork")) {
  				XmlClass tc27 = new XmlClass(TC27_Toolkit_SaveMyWork.class);
				list.add(tc27);		
			}
  			if (arrayValue.get(i).equals("TC28_Toolkit_SaveAndExit")) {
  				XmlClass tc28 = new XmlClass(TC28_Toolkit_SaveAndExit.class);
				list.add(tc28);	
  			}
  			if (arrayValue.get(i).equals("TC29_Search_SCRId")) {
  				XmlClass tc29 = new XmlClass(TC29_Search_SCRId.class);
				list.add(tc29);	
			}
  			if (arrayValue.get(i).equals("TC30_Search_SCRName")) {
  				XmlClass tc30 = new XmlClass(TC30_Search_SCRName.class);
				list.add(tc30);	
			}
  			
  			
  			
  		}
  		
  		
  		System.out.println(list);
		
  		
  		
		xmltest.setXmlClasses(list);
		TestNG testng =new TestNG();
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(xmlsuite);
		testng.setXmlSuites(suites);
		testng.run();

		connection.close();
		rs.close();
	}

	public XmlSuite appendXmlTest(XmlSuite xmlSuite) {
	    XmlTest xmlTest = new XmlTest(xmlSuite);    // initialise a xml test instance with XmlSuite instance
	    // below is initialise `xmlTest` instance context, for example, testing class, testing method of testing     class
	  
	    return xmlSuite;
	}
	
}



