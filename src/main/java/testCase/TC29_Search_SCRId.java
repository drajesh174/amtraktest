package testCase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

public class TC29_Search_SCRId extends BaseClass {

	
	@Test
	public void serviceCancellation() throws Exception {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC29_Search_SCRId") && runStatus.equals("Yes")) {
				
				System.out.println("**********Execution Advanced Seach by SCR_ID*********");
				TimeUnit.SECONDS.sleep(5);
				b=true;
				lp.LoginDetails();
				Thread.sleep(2000);
				sp.fn_searchSCRId();
				
			}
			rs.next();
		}
		
	}
	
	
	
	
	
}
