package testCase;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.testng.annotations.Test;

public class TC35_VerifyPageTitle extends BaseClass {
	
	
	@Test
	public void VerifyTitle() throws Exception{
		
		int cnt = rs.getCount();
		rs.moveFirst();
		System.out.println(cnt);

		String firstPage = "Details - Schedule Management";
		String secoundPage = "Impact - Schedule Management";
		String thirdPage = "Pre Impact summary - Schedule Management";
		String forthPage = "Schedule Management";
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");	
			if(testname.equals("TC35_VerifyPageTitle") && runStatus.equals("Yes")) {
				System.out.println("Executing End to End Test Case ");
				TimeUnit.SECONDS.sleep(5);
				
				lp.LoginDetails();
				Thread.sleep(2000);
				crp.fn_EnterServiceNO();
				Assert.assertEquals(firstPage, driver.getTitle());
				cp.fn_SelectStartDate();
				cp.fn_ClickCancellationTypeAndCity();
				cp.fn_SelectReasonType();
				Assert.assertEquals(firstPage, driver.getTitle());
				cp.fn_addComment();
				cp.fn_clickContinue();
				isp.fn_CustPNRCnt();// PNR count
				isp.fn_getScaid();
				isp.fn_ValidateSDHApi(); // Validate SDH API
				isp.fn_Reaccomodation(); // Re-accommodation LookUp
				Assert.assertEquals(secoundPage, driver.getTitle());
				isp.fn_clickContinueImp();
				psp.fn_clickSubmit();
				Assert.assertEquals(thirdPage, driver.getTitle());
				psp.fn_closePopp();
				Assert.assertEquals(forthPage, driver.getTitle());		
		
	}

}
		
		
		
}
	
	
}