package testCase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pages.CancellationDetailsPage;
import pages.CreateRequestPage;
import pages.ImpactSummaryPage;
import pages.LoginPage;
import pages.NotifyPage;
import pages.PreImpactSummaryPage;
import pages.SearchPage;

public class BaseClass 
{
	public static WebDriver driver;
	public static ExtentReports reports; 
	public static ExtentTest logger;
	public static Recordset rs;
	public static com.codoid.products.fillo.Connection connection;
	public static boolean b;
	CancellationDetailsPage cp;
	LoginPage lp;
	CreateRequestPage crp;
	ImpactSummaryPage isp;
	PreImpactSummaryPage psp;
	SearchPage sp;
	NotifyPage np;
	public Properties prop = new Properties();
	
	String testName = this.getClass().getSimpleName();
	
	@BeforeSuite(alwaysRun = true)
	public void launchUrl() throws Exception{
		Runtime.getRuntime().exec("cmd /c taskkill /f /im excel.exe");
		String filepath = System.getProperty("user.dir") + "\\" + "data.properties";
		File file = new File(filepath);
		FileInputStream fileinput = new FileInputStream(file);
		prop.load(fileinput);
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeOptions cOption = new ChromeOptions();
		cOption.addArguments("disable-infobars");
		driver = new ChromeDriver(cOption);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("Test_url"));  // Test Environment
		//driver.get(prop.getProperty("Dev_url"));  // Dev Environmen
		
	}
		
	
		@BeforeClass(alwaysRun = true)
		public void LoadTestData() throws FilloException{
			
			b=false;
		//----------------Extent Report----------
		reports = GetExtent();
		logger = reports.startTest(testName);
		Fillo fillo = new Fillo();
		connection = fillo.getConnection(".\\TestData\\testdata.xlsx");
		String strQuery = "Select * from Sheet1 where Run = 'Yes'";
		rs=connection.executeQuery(strQuery);
		
		//------------- reference for Page class---------------
		//p1 = new Page1(driver,logger,rs);
		cp = new CancellationDetailsPage(driver, logger, rs);
		lp = new LoginPage(driver, logger, rs);
		crp = new CreateRequestPage(driver, logger, rs);
		//dp = new DashboardPage(driver, logger, rs);
		isp = new ImpactSummaryPage(driver, logger, rs);
		psp = new PreImpactSummaryPage(driver, logger, rs);
		sp = new SearchPage(driver, logger, rs);
		np = new NotifyPage(driver, logger, rs);
		rs.next();
	}
	
	public static synchronized ExtentReports GetExtent() {
		
		if(reports != null) return reports;
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		
		String tdate = dateFormat.format(date);
		return reports = new ExtentReports(".\\Reports\\Test_Run Report_"+tdate+".html");
		
	}
	
	
	@AfterClass(alwaysRun = true)
	public void FlushReport() {
		
		if(b==false) {
			logger.log(LogStatus.SKIP, "TestCase has been Skipped");
		}
		
		logger.log(LogStatus.INFO, "Test Run completed");
		//logger.log(LogStatus.INFO, "StackTrace Result: " + Thread.currentThread().getStackTrace());
		reports.flush();
		//connection.close();
		
	}
	
	@AfterSuite(alwaysRun = true)
	public void closeDriver() {
		driver.close();
		driver.quit();
	}
	}

