package testCase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

public class TC24_ContactCentreAgent_Addcomment_NotifyCustomer extends BaseClass {

	
	@Test
	public void verifyServiceNumber() throws Exception {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC24_ContactCentreAgent_Addcomment_NotifyCustomer") && runStatus.equals("Yes")) {
				
				b=true;
				System.out.println("Executing End to End Test Case ");
				TimeUnit.SECONDS.sleep(5);
				lp.LoginDetails();
				System.out.println("Click on Create menu");
				Thread.sleep(2000);
				sp.fn_searchSCRId();
				np.fn_ChangeToComplete();
				isp.fn_validateEmail();
				
			}
			rs.next();
		}
		
	}
	
	
	
	
}
