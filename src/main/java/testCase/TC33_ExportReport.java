package testCase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

public class TC33_ExportReport extends BaseClass {
	
	
	@Test
	public void validateDiscard() throws Exception{
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			if(testname.equals("TC33_ExportReport") && runStatus.equals("Yes")) {
				
				System.out.println("********* Execution started for Discard functionality*********");
				TimeUnit.SECONDS.sleep(5);
				lp.LoginDetails();
				Thread.sleep(8000);
				System.out.println("Click on Create menu");
				sp.fn_searchSCRStatus();
				Thread.sleep(2000);
				sp.fn_generateReport();
		
	}
			rs.next();
	
		}
	

}
	}
