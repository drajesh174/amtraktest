package testCase;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;

public class TC13_SingleDateFull_WF3_Planned extends BaseClass {

	
	@Test
	public void verifyServiceNumber() throws Exception {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC13_SingleDateFull_WF3_Planned") && runStatus.equals("Yes")) {
				
				b=true;
				
			
				
				System.out.println("Executing End to End Test Case ");
				TimeUnit.SECONDS.sleep(5);
				lp.LoginDetails();
				System.out.println("Click on Create menu");
				Thread.sleep(2000);
				crp.fn_EnterServiceNO();
				
				cp.fn_SelectStartDate();
				cp.fn_ClickCancellationTypeAndCity();
				cp.fn_SelectReasonType();
				cp.fn_addComment();
				cp.fn_clickContinue();
				isp.fn_CustPNRCnt(); // PNR count
				isp.fn_getScaid();
				isp.fn_ValidateSDHApi(); // Validate SDH API
				isp.fn_Reaccomodation(); // Re-accommodation LookUp
				isp.fn_clickContinueImp();
				psp.fn_clickSubmit();
				psp.fn_closePopp();
				
				
				//-----------------------------
				Thread.sleep(10000);
				isp.fn_ValidateTicketApi();
				isp.fn_getScaIdAndDBValidation();  // Validate Status
				isp.fn_validateEmail();
			    isp.fn_updateExcel();
			
			
				
			}
			rs.next();
		}
		
	}
	
	
	
	
}
