package testCase;

import java.awt.AWTException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;




public class TC21_ValidateServiceNumber extends BaseClass{

	
	
	@Test
	public void verifyServiceNumber() throws FilloException, InterruptedException, AWTException, IOException {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC21_ValidateServiceNumber") && runStatus.equals("Yes")) {
				
	            b=true;
		
				System.out.println("Executing TC 21");
				TimeUnit.SECONDS.sleep(5);
				lp.LoginDetails();
				Thread.sleep(2000);
				crp.fn_validateServiceNo();
				
				
			}
			rs.next();
		}
		
	}
	
	
	
	
}
