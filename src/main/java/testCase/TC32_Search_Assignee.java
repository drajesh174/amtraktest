package testCase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

public class TC32_Search_Assignee extends BaseClass {

	
	@Test
	public void verifyServiceNumber() throws Exception {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC32_Search_Assignee") && runStatus.equals("Yes")) {
				
				
				System.out.println("**********Execution Advanced Seach by Service Name*********");
				TimeUnit.SECONDS.sleep(5);
				b=true;
				lp.LoginDetails();
				Thread.sleep(2000);
				sp.fn_searchAssignee();
		
			
				
			}
			rs.next();
		}
		
	}
	
	
	
	
}
