package testCase;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;

public class TC23_ValidateDiscardFunctionality extends BaseClass {

	
	@Test
	public void verifyServiceNumber() throws FilloException, InterruptedException, AWTException, ParseException, IOException {
		
		int cnt = rs.getCount();
		rs.moveFirst();
		
		System.out.println(cnt);
		
		for(int i=0;i<cnt;i++) {
			
			String testname = rs.getField("TestCase_Name");
			String runStatus = rs.getField("Run");
			
			System.out.println(testname);
			System.out.println(runStatus);
			
			if(testname.equals("TC23_ValidateDiscardFunctionality") && runStatus.equals("Yes")) {
				
	            b=true;
			
				System.out.println("Executing TC 23 ");
				TimeUnit.SECONDS.sleep(5);
				lp.LoginDetails();
				System.out.println("Click on Create menu");
				Thread.sleep(2000);
				crp.fn_EnterServiceNO();
				
				cp.fn_SelectStartDate();
				cp.fn_ClickCancellationTypeAndCity();
				cp.fn_SelectReasonType();
				cp.fn_addComment();
				cp.fn_validateDiscard();
				
			}
			rs.next();
		}
		
	}
	
	
	
	
}
